<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
	protected $guarded = [];
    public function messages(){
        return $this->belongsTo(Message::class, 'message_id');
    }
}
