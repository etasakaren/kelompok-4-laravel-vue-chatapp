<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       \App\Models\User::factory()->create(
        [
            'email' => 'hasan@gmail.com',
            'name' => 'hasan'
        ]
    );
       \App\Models\User::factory()->create(
        [
            'email' => 'hasna@gmail.com',
            'name' => 'hasna'
        ]
    );
       \App\Models\User::factory()->create(
        [
            'email' => 'ayu@gmail.com',
            'name' => 'ayu'
        ]
    );
    \App\Models\User::factory()->create(
        [
            'email' => 'etak77@gmail.com',
            'name' => 'etak',
            'password'  => bcrypt('password')
        ]
    );
    \App\Models\User::factory()->create(
        [
            'email' => 'karen@gmail.com',
            'name' => 'karen',
            'password'  => bcrypt('password')
        ]
    );
   }
}
