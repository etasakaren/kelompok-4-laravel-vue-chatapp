<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


// Route::post('getFriends', 'App\Http\Controllers\HomeController@getFriends');

Route::post('/getFriends', 'App\Http\Controllers\HomeController@getFriends');
Route::post('/session/create', 'App\Http\Controllers\SessionController@create');
Route::post('/session/{session}/chats', 'App\Http\Controllers\ChatController@chats');
Route::post('/send/{session}', 'App\Http\Controllers\ChatController@send');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
